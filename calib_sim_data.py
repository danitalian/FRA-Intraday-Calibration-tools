import multiprocessing
import random
from typing import Any, Callable, Dict, List, Tuple, Union

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotnine
import scipy
from plotnine import *
from scipy.optimize import minimize
from xgboostlss.distributions.Gaussian import *
from xgboostlss.distributions.Laplace import *
from xgboostlss.model import *


def main():
    locs_true = [0.1, 0.5, 1]
    scales_true = [0.5, 1.5]
    n_samples = 6000
    x_locs = np.linspace(30, 140, n_samples)
    x_scales = np.linspace(0.1, 0.9, n_samples)
    buckets_loc = [[(30, 60)], [(60, 90)], [(90, 140)]]
    buckets_scale = [[(0.1, 0.3), (0.7, 0.9)], [(0.3, 0.7)]]
    y = [0 for k in range(n_samples)]
    locs_true_x = [0 for k in range(n_samples)]
    scales_true_x = [0 for k in range(n_samples)]

    def find_buckets(x_loc, x_scale):
        found = False
        for k in range(len(buckets_loc)):
            buck = buckets_loc[k]
            for subbuck in buck:
                if x_loc >= subbuck[0] and x_loc <= subbuck[1]:
                    x_loc_buck = k
                    found = True
                    break
            if found:
                break
        found = False
        for k in range(len(buckets_scale)):
            buck = buckets_scale[k]
            for subbuck in buck:
                if x_scale >= subbuck[0] and x_scale <= subbuck[1]:
                    x_scale_buck = k
                    found = True
                    break
            if found:
                break

        return x_loc_buck, x_scale_buck

    for sample in range(n_samples):
        x_loc_buck, x_scale_buck = find_buckets(x_locs[sample], x_scales[sample])
        y[sample] = np.random.normal(locs_true[x_loc_buck], scales_true[x_scale_buck])
        locs_true_x[sample] = locs_true[x_loc_buck]
        scales_true_x[sample] = scales_true[x_scale_buck]

    # This works only if we know the "Buckets" a priori
    def neg_log_likelihood(
        params: List[float],
        data_obs: List[float],
        locs_driver,
        scales_driver,
        distribution: Callable,
    ) -> float:
        # Length of our timeline (number of values of t), and list of names of drivers:
        length = len(data_obs)

        # Loop through our time periods, and calculate locs and scales for each t:
        probabilities = np.zeros(length)
        locs, scales = np.zeros(length), np.zeros(length)
        for t in range(length):
            loc_buck, scale_buck = find_buckets(locs_driver[t], scales_driver[t])
            locs[t] += params[loc_buck]
            scales[t] += params[scale_buck + len(locs_true)]
            # Calculate the pdf at each observed data point using the loc and scale we just found,
            # with the distribution we input:
            probabilities[t] = distribution(data_obs[t], locs[t], scales[t])
        # Sum of the logs of the probabilities:
        n_l_l = -np.sum(np.log(probabilities))
        return n_l_l

    results = minimize(
        neg_log_likelihood,
        x0=[0.3, 0.1, 0.5, 0.4, 0.4],
        args=(y, x_locs, x_scales, scipy.stats.norm.pdf),
        method="Nelder-Mead",
    )
    print(results.x)

    # XGboost + gamlss (https://github.com/StatMixedML/XGBoostLSS) uses boosting to find the link between drivers and sample distribution parameters without extra info (it doesn't know the buckets in this application)
    # It poses problem when the training set doesn't include all potential domains of interest for the drivers

    # randomize order of samples first
    df = pd.DataFrame(
        data={
            "x_loc": x_locs,
            "x_scale": x_scales,
            "loc": locs_true_x,
            "scale": scales_true_x,
            "y": y,
        }
    )
    random_index = random.sample(range(len(y)), len(y))
    df = df.iloc[random_index, :]

    n_cpu = multiprocessing.cpu_count()
    n_train = int(round(n_samples * 0.6))

    df_train = df.iloc[:n_train, :]
    df_test = df.iloc[n_train:, :]

    X_train, y_train = df_train.filter(regex="x"), df_train["y"].values
    X_test, y_test = df_test.filter(regex="x"), df_test["y"].values

    dtrain = xgb.DMatrix(X_train, label=y_train, nthread=n_cpu)
    dtest = xgb.DMatrix(X_test, nthread=n_cpu)

    xgblss = XGBoostLSS(
        Gaussian(stabilization="None", response_fn="exp", loss_fn="nll")
    )
    param_dict = {
        "eta": ["float", {"low": 1e-5, "high": 1, "log": True}],
        "max_depth": ["int", {"low": 1, "high": 10, "log": False}],
        "gamma": ["float", {"low": 1e-8, "high": 40, "log": True}],
        "subsample": ["float", {"low": 0.2, "high": 1.0, "log": False}],
        "colsample_bytree": ["float", {"low": 0.2, "high": 1.0, "log": False}],
        "min_child_weight": ["float", {"low": 1e-8, "high": 500, "log": True}],
        "booster": ["categorical", ["gbtree"]],
    }

    np.random.seed(123)
    opt_param = xgblss.hyper_opt(
        param_dict,
        dtrain,
        num_boost_round=100,  # Number of boosting iterations.
        nfold=5,  # Number of cv-folds.
        early_stopping_rounds=20,  # Number of early-stopping rounds
        max_minutes=10,  # Time budget in minutes, i.e., stop study after the given number of minutes.
        n_trials=30,  # The number of trials. If this argument is set to None, there is no limitation on the number of trials.
        silence=True,  # Controls the verbosity of the trail, i.e., user can silence the outputs of the trail.
        seed=123,  # Seed used to generate cv-folds.
        hp_seed=123,  # Seed for random number generator used in the Bayesian hyperparameter search.
    )
    np.random.seed(123)
    opt_params = opt_param.copy()
    n_rounds = opt_params["opt_rounds"]
    del opt_params["opt_rounds"]

    # Train Model with optimized hyperparameters
    xgblss.train(opt_params, dtrain, num_boost_round=n_rounds)
    pred_params = xgblss.predict(dtest, pred_type="parameters")

    pred_params["x_scale"] = X_test["x_scale"].values
    dist_params = ["scale"]

    # Data with actual values
    plot_df_actual = pd.melt(
        df_test[["x_scale"] + dist_params], id_vars="x_scale", value_vars=dist_params
    )
    plot_df_actual["type"] = "TRUE"

    # Data with predicted values
    plot_df_predt = pd.melt(
        pred_params[["x_scale"] + dist_params],
        id_vars="x_scale",
        value_vars=dist_params,
    )
    plot_df_predt["type"] = "PREDICT"

    plot_df = pd.concat([plot_df_predt, plot_df_actual])

    plot_df["variable"] = plot_df.variable.str.upper()
    plot_df["type"] = pd.Categorical(plot_df["type"], categories=["PREDICT", "TRUE"])

    g1 = (
        ggplot(plot_df, aes(x="x_scale", y="value", color="type"))
        + geom_line(size=1.1)
        + facet_wrap("variable", scales="free")
        + labs(
            title="Parameters of univariate Gaussian predicted with XGBoostLSS",
            x="",
            y="",
        )
        + theme_bw(base_size=15)
        + theme(
            legend_position="bottom",
            plot_title=element_text(hjust=0.5),
            legend_title=element_blank(),
        )
    )
    g1.draw(show=True)

    pred_params["x_loc"] = X_test["x_loc"].values
    dist_params = ["loc"]

    # Data with actual values
    plot_df_actual = pd.melt(
        df_test[["x_loc"] + dist_params], id_vars="x_loc", value_vars=dist_params
    )
    plot_df_actual["type"] = "TRUE"

    # Data with predicted values
    plot_df_predt = pd.melt(
        pred_params[["x_loc"] + dist_params], id_vars="x_loc", value_vars=dist_params
    )
    plot_df_predt["type"] = "PREDICT"

    plot_df = pd.concat([plot_df_predt, plot_df_actual])

    plot_df["variable"] = plot_df.variable.str.upper()
    plot_df["type"] = pd.Categorical(plot_df["type"], categories=["PREDICT", "TRUE"])

    g2 = (
        ggplot(plot_df, aes(x="x_loc", y="value", color="type"))
        + geom_line(size=1.1)
        + facet_wrap("variable", scales="free")
        + labs(
            title="Parameters of univariate Gaussian predicted with XGBoostLSS",
            x="",
            y="",
        )
        + theme_bw(base_size=15)
        + theme(
            legend_position="bottom",
            plot_title=element_text(hjust=0.5),
            legend_title=element_blank(),
        )
    )
    g2.draw(show=True)


if __name__ == "__main__":
    main()
