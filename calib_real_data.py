import datetime
import os
from pathlib import Path
from typing import Any, Callable, Dict, List, Tuple, Union

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy
from fitter import Fitter
from xgboostlss.model import *

matplotlib.use("TkAgg")
OUTPUTS_FOLDER = "Outputs"


def main():

    if not os.path.exists(OUTPUTS_FOLDER):
        os.makedirs(OUTPUTS_FOLDER)

    df = pd.read_csv("Inputs\dataset.csv")
    df["ID3"] = df["ID3"].ffill().bfill()
    df["DA price"] = df["DA price"].ffill().bfill()
    df["ID3_DA_premium"] = df["ID3"] - df["DA price"]
    df["timestamp"] = df["timestamp"].apply(
        lambda t: datetime.datetime.strptime(t[:-6], "%Y-%m-%d %H:%M:%S")
    )
    # df["RES LF"] = df["total RES Generation (MWh)"] / df["RES Installed Capacity"]
    # df["Solar LF"] = df["Solar Generation (MWh)"] / df["Solar Installed Capacity"]
    # df["Wind LF"] = df["Wind Generation (MWh)"] / df["Wind Installed Capacity"]
    # df["total RES LF Forecast Abs error"] = (
    #     df["total RES DA Forecast error (MWh)"] / df["RES Installed Capacity"]
    # ).apply(abs)
    # df["Solar LF Forecast Abs error"] = (
    #     df["Solar DA forecast error (MWh)"] / df["Solar Installed Capacity"]
    # ).apply(abs)
    # df["Wind LF Forecast Abs error"] = (
    #     df["Wind DA forecast error (MWh)"] / df["Wind Installed Capacity"]
    # ).apply(abs)
    # df["Residual Demand"] = df["Total Demand (MWh)"] - df["total RES Generation (MWh)"]
    # df["Residual Demand No Nuclear"] = (
    #     df["Residual Demand"] - df["Nuclear Generation (MWh)"]
    # )

    n_quantiles = 10

    ##autocorrelation first
    def autocorr_coef(y: List[float], k: int) -> float:
        """
        Calculate the autocorrelation coefficient of a data set for a given lag.
        Methodology: https://docs.oracle.com/cd/E57185_01/CBREG/ch06s03s03s03.html

        :param y: the data we are testing.
        :param k: the lag for which we will calculate the coefficient.

        :return r_k: the autocorrelation coefficient.
        """
        # The following calculations simply replicate the formula in the methodology:
        y_bar = np.average(y)
        denom = np.sum([(y - y_bar) ** 2 for y in y])
        # If denom is zero, that means the data is just a series of y_bars, which has
        # an autocorrelation coefficient of 1:
        if denom == 0:
            return 1
        numer = 0
        for t in range(k, len(y)):
            numer += (y[t] - y_bar) * (y[t - k] - y_bar)
        r_k = numer / denom

        return r_k

    def autocorrelation(data: List[float], lags: Union[int, List[int]]) -> List[float]:
        """
        Calculate the autocorrelation coefficients of a data set for a set of lags.
        Methodology: https://docs.oracle.com/cd/E57185_01/CBREG/ch06s03s03s03.html

        :param data: the data we are testing.
        :param lags: the set of "lags" for which we will calculate the coefficients:
        - if list, will calculate coefficients for every lag in the list
        - if int, will calculate coefficients for every lag in {0, 1, ... , lags}

        :return r: a list of autocorrelation coefficients, such that r[i] corresponds to a
        lag of lags[i].
        """
        # If integer lags, turn it into a list from zero to its value:
        if type(lags) == int:
            assert lags >= 0, f"Integer lags input ({lags}). Must be non-negative."
            lags = list(range(lags + 1))

        # Calculate the coefficient for each lag:
        r = np.zeros(len(lags))
        for i, lag in enumerate(lags):
            r[i] = autocorr_coef(data, lag)

        return r

    rho = autocorr_coef(list(df["ID3_DA_premium"]), 1)
    rho_python = np.corrcoef(df["ID3_DA_premium"][1:], df["ID3_DA_premium"][:-1])
    kendall_tau = scipy.stats.kendalltau(
        df["ID3_DA_premium"][1:], df["ID3_DA_premium"][:-1]
    )
    df["year"] = df["timestamp"].apply(lambda x: x.year)
    df["DA price standardised"] = df["DA price"]
    df["quantile DA price standardised"] = pd.qcut(
        df["DA price standardised"], q=n_quantiles, precision=0, labels=False
    )
    df = df.groupby(by=["quantile DA price standardised"]).apply(lambda x: x)
    quantiles_bins = df["quantile DA price standardised"].unique()
    fitted_params_df = pd.DataFrame(data={"bin": quantiles_bins, "loc": 0, "scale": 0})
    for n_q in range(n_quantiles):
        fig, ax = plt.subplots()
        f = Fitter(
            df.loc[(quantiles_bins[n_q],), "ID3_DA_premium"].values,
            distributions=["norm", "laplace"],
        )
        f.fit()
        best_params = list(f.get_best().values())[0]
        fitted_params_df.loc[n_q, "loc"] = best_params["loc"]
        fitted_params_df.loc[n_q, "scale"] = best_params["scale"]
        print(f.summary())
        plt.title(f"ID3-DA premium distribution bin {quantiles_bins[n_q]}")

        file_name = f"ID3-DA premium distribution bin {quantiles_bins[n_q]}.jpg"
        plt.savefig(Path(OUTPUTS_FOLDER).joinpath(file_name))

    fitted_params_df["kendall_tau"] = kendall_tau.correlation
    fitted_params_df.to_csv(Path(OUTPUTS_FOLDER).joinpath("fitted_params.csv"))
    # plt.show(block=True)  # uncomment to show plots and pause program


if __name__ == "__main__":
    main()
