from functools import partial
from pathlib import Path
from typing import Any, Callable

import numpy as np
import pandas as pd
from scipy.stats import laplace, norm

SEED = 340


def main():
    id_params_df = pd.read_csv("Inputs\id_params.csv")
    id_params_dict = {
        "loc": id_params_df["loc"],
        "scale": id_params_df["scale"],
        "kendall_tau": id_params_df["kendall_tau"].iloc[0],
        "quantile_trunc": 0.05,
    }
    prices_read = pd.read_csv("Inputs\histDA_histID3.csv")
    seeds = [k for k in range(10000)]
    # for sd in seeds:
    #     IDM = IntradayMarket("FRA", prices_read["DA price"].iloc[:24*366], id_params_dict, sd)
    #     IDM.run()
    #     if abs(IDM.premia.median()-(-0.14))<0.06 and abs(IDM.premia.mean()-0.98)<0.4:
    #         print(f"SEED={sd}")
    #         break
    IDM = IntradayMarket(
        "FRA", prices_read["DA price"].iloc[: 24 * 366], id_params_dict, SEED
    )
    IDM.run()
    # prices_write = prices_read.copy()
    # prices_write["ID3"] = IDM.id_prices.values
    # prices_write["ID3-DA"] = IDM.premia
    print(
        f"Historic stats \n {prices_read['ID3-DA'].describe()} \n corr coeff H,H+1 = {np.corrcoef(prices_read['ID3-DA'].iloc[:-1], prices_read['ID3-DA'].iloc[1:])[0,1]}"
    )
    print(
        f" \n \n Sample stats \n {IDM.premia.describe()} \n corr coeff H,H+1 = {np.corrcoef(IDM.premia.iloc[:-1], IDM.premia.iloc[1:])[0,1]}"
    )
    IDM.id_prices.to_csv(r"Outputs\histDA_simID3.csv", index=True)


class IntradayMarket:
    """Intraday Market class. Processor to determine ID prices."""

    def __init__(
        self,
        region: str,
        da_prices: pd.Series,
        distr_par: dict,
        seed_rng,
        **kwargs,
    ):
        """Initialise."""
        self.region: str = region

        self.da_prices: pd.Series = da_prices
        self.n_quantiles = len(distr_par["loc"])
        self.kendall_tau = distr_par["kendall_tau"]
        self.truncate_quantile_bound = distr_par["quantile_trunc"]
        self.locs = distr_par["loc"]
        self.scales = distr_par["scale"]

        if kwargs:
            raise KeyError(f"Unused kwargs: {kwargs.keys()}")

        self.premia = pd.Series(index=self.da_prices.index)
        self.id_prices = pd.Series(index=self.da_prices.index)

        # set seed to random generator for reproducibility
        self.rng = np.random.default_rng(seed=seed_rng)

    def run(self):
        """Run all calculations."""
        self._generate_id_prices()

    def _generate_id_prices(self):
        """Generate prices according to distribution parameters."""
        uniform_copulas = self._generate_gaussian_copula()
        quantiles = pd.qcut(
            self.da_prices, q=self.n_quantiles, precision=0, labels=False
        )
        for n_q in range(self.n_quantiles):
            loc = self.locs.iloc[n_q]
            scale = self.scales.iloc[n_q]
            index_samples = quantiles[quantiles == n_q].index
            self.premia[index_samples] = laplace.ppf(
                uniform_copulas[index_samples], loc=loc, scale=scale
            )
        # adjust the premia to be zero mean in each year
        # self.premia = self.premia - self.premia.mean()
        self.id_prices = self.da_prices + self.premia
        return

    def _generate_gaussian_copula(self):
        """Generate uniforms through gaussian copula to model dependence. (See https://en.wikipedia.org/wiki/Copula_(probability_theory) and https://people.math.ethz.ch/~embrecht/ftp/copchapter.pdf)"""
        normal_sampler = partial(self.rng.normal, loc=0, scale=1)
        bound = abs(norm.ppf(self.truncate_quantile_bound, loc=0, scale=1))
        seed = truncate_and_sample_distribution(normal_sampler, bound)
        rho_from_kendal_tau = np.sin(self.kendall_tau * np.pi / 2)
        size = len(self.da_prices)
        normal_samples = list(
            sampling_generator(size, seed, rho_from_kendal_tau, bound, normal_sampler)
        )
        uniform_copulas = pd.Series(
            index=self.id_prices.index, data=norm.cdf(normal_samples)
        )
        return uniform_copulas


def truncate_and_sample_distribution(
    fn: Callable[[Any], float], bound: float, max_it: int = 1000, **kwargs
) -> float:
    """
    Sample from a chosen distribution, truncated above and below at a given value.
    Resamples from the given distribution (rather than clipping).
    :param fn: callable to be sampled. Should be a random sampler e.g. np.random.normal. Must return a float.
    :param bound: abs value of bound to be placed on function being sampled.
    :param max_it: max number of iterations before aborting
    :kwargs: arguments for fn

    :return: float randomly sampled from the truncated distribution
    """
    x = fn(**kwargs)
    if not isinstance(x, float):
        raise TypeError(
            f"Function passed must return a float. Return type is {type(x).__name__}."
        )
    iteration = 0
    while not -bound <= x <= bound and iteration < max_it:
        x = fn(**kwargs)
        iteration += 1
    if iteration >= max_it:
        raise RuntimeError(
            f"Function did not return a valid value after {max_it} iterations. Check the arguments."
        )
    return x


def sampling_generator(size: int, seed: float, rho: float, bound: float, sampler):
    x = seed
    _sqrt_precompute = np.sqrt(1 - rho**2)
    yield x
    for _ in range(size - 1):
        x = x * rho + _sqrt_precompute * truncate_and_sample_distribution(
            sampler, bound
        )
        yield x


if __name__ == "__main__":
    main()
