import os
from pathlib import Path

import numpy as np
import pandas as pd

OUTPUTS_FOLDER = "Outputs"
INPUTS_FOLDER = "Inputs"
ID3_FOLDER_NAME = "Continuous Trades Intraday"


def main():

    if not os.path.exists(OUTPUTS_FOLDER):
        os.makedirs(OUTPUTS_FOLDER)
    ID3_FOLDER = Path.cwd() / INPUTS_FOLDER / ID3_FOLDER_NAME

    df_list = []
    for filename in os.listdir(ID3_FOLDER):
        zip_filepath = ID3_FOLDER / filename
        df_list.append(
            pd.read_csv(
                zip_filepath,
                header=1,
                usecols=[
                    "TradeId",
                    "DeliveryStart",
                    "DeliveryEnd",
                    "ExecutionTime",
                    "Price",
                    "Volume",
                ],
            )
        )
    df = pd.concat(df_list)
    df = df.sort_values(by=["TradeId", "ExecutionTime"])
    df = df.drop_duplicates(subset=["TradeId"], keep="last")
    df[["DeliveryStart", "DeliveryEnd", "ExecutionTime"]] = df[
        ["DeliveryStart", "DeliveryEnd", "ExecutionTime"]
    ].apply(lambda x: pd.to_datetime(x))
    df = df[
        ~(
            (df["DeliveryStart"].apply(lambda x: x.minute) > 0)
            | (df["DeliveryEnd"].apply(lambda x: x.minute) > 0)
        )
    ]
    df["hours_to_delivery"] = (df["DeliveryStart"] - df["ExecutionTime"]).apply(
        lambda x: np.floor(x.seconds / 3600)
    )
    df = df[df["hours_to_delivery"] < 3]

    def wavg(group, avg_name, weight_name):
        d = group[avg_name]
        w = group[weight_name]
        return (d * w).sum() / w.sum()

    id3 = df.groupby(by="DeliveryStart").apply(wavg, "Price", "Volume")
    id3.to_csv(Path.cwd() / OUTPUTS_FOLDER / "id3_historical.csv")


if __name__ == "__main__":
    main()
